FROM golang:1.22 as builder

# Set the working directory inside the container
WORKDIR /go/src/app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -v -o app .

# Use a Docker multi-stage build to create a lean production image
# https://docs.docker.com/develop/develop-images/multistage-build/
FROM alpine:latest  
RUN apk --no-cache add ca-certificates

WORKDIR /root/

# Copy the binary from the builder stage
COPY --from=builder /go/src/app/app .

# Command to run the executable
CMD ["./app"]


