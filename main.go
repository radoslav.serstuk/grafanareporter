package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
)

type GraphanaPage struct {
	URL     string
	PDFName string
}

func main() {
	var pages []GraphanaPage
	var wg sync.WaitGroup

	baseCtx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	baseCtx, cancel = context.WithTimeout(baseCtx, 200*time.Second)
	defer cancel()

	urlsFilePath := filepath.Join("/etc/config", "urls")
	namesFilePath := filepath.Join("/etc/config", "names")

	//urlsFilePath := filepath.Join("C:\\config", "urls") //--- to test in windows
	//namesFilePath := filepath.Join("C:\\config", "names")

	urlsData, err := os.ReadFile(urlsFilePath)
	if err != nil {
		panic(err)
	}
	namesData, err := os.ReadFile(namesFilePath)
	if err != nil {
		panic(err)
	}

	urls := strings.Split(string(urlsData), ",")
	names := strings.Split(string(namesData), ",")

	if len(urls) != len(names) {
		panic("The number of URLs and PDF names must be equal")
	}

	for i := range urls {
		pages = append(pages, GraphanaPage{URL: strings.TrimSpace(urls[i]), PDFName: strings.TrimSpace(names[i])})
	}

	wg.Add(len(pages))

	for _, page := range pages {
		go func(page GraphanaPage) {
			defer wg.Done()
			ctx, cancel := chromedp.NewContext(baseCtx)
			defer cancel()
			graphanaHandler(ctx, page.URL, page.PDFName)
		}(page)
	}

	wg.Wait()

}

func heightCalculator(elements []string) int {
	var lastElementHeight int
	failedElem := 0
	re := regexp.MustCompile(`width: (\d+)px; height: (\d+)px;`)
	for _, element := range elements {
		matches := re.FindStringSubmatch(element)
		if len(matches) == 3 {
			height, err := strconv.Atoi(matches[2])
			if err != nil {
				log.Fatalf("Failed to convert height: %v", err)
			}
			lastElementHeight = lastElementHeight + height + 30
		}
	}
	if failedElem != 0 {
		temp := int(lastElementHeight / (len(elements) - failedElem))
		lastElementHeight = lastElementHeight + (temp * failedElem)
	}

	return lastElementHeight
}

func graphanaHandler(ctx context.Context, url, pdfName string) {

	width, height := 750, 1000

	selector := `[data-panelid]`
	var elements []string
	var buf []byte

	err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		chromedp.Sleep(3*time.Second),
		chromedp.Evaluate(fmt.Sprintf(`Array.from(document.querySelectorAll('%s')).map(el => el.outerHTML);`, selector), &elements),
	)
	if err != nil {
		log.Fatalf("Failed to execute task: %v", err)
	}

	height = heightCalculator(elements)

	err = chromedp.Run(ctx,
		chromedp.EmulateViewport(int64(width), int64(height)),
		chromedp.Sleep(5*time.Second),
		chromedp.ActionFunc(func(ctx context.Context) error {
			var err error
			heightInch := height / 94
			buf, _, err = page.PrintToPDF().WithPrintBackground(false).WithPaperHeight(float64(heightInch)).WithPaperWidth(float64(8)).Do(ctx)
			return err
		}),
	)
	if err != nil {
		log.Fatalf("Failed to execute task: %v", err)
	}

	if err := os.WriteFile(pdfName+".pdf", buf, 0644); err != nil {
		log.Fatalf("Failed to write PDF: %v", err)
	}
}
